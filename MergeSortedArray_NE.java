package LeetcodeArrayEasy;

public class MergeSortedArray {

	
	private static void merge(int[] nums1, int[] nums2, int m, int n) {
		
		// Make a copy of nums1.
		int[] nums1_copy = new int[m];
		
		System.arraycopy(nums1, 0, nums1_copy, 0, m);
		
       // Two get pointers for nums1_copy and nums2.
		int p1 = 0;
	    int p2 = 0;
	    
	    int i=0;
	    
	    while((p1<m) && (p2<n)) {
	    	
	    	if(nums1_copy[p1] <nums2[p2]) {
	    		nums1[i++] = nums1_copy[p1++];
	    	}
	    	
	    	else {
	    		nums1[i++] = nums2[p2++];
	    	}
	    	
	    	
	    	if(p1<m) {
	    		System.arraycopy(nums1_copy, p1, nums1, p1+p2, m+n-(p1+p2));
	    	}
	    		
	    	if(p2<n) {
	    		System.arraycopy(nums2, p2, nums1, p1+p2, m+n-(p1+p2));
	    	}
	    }
	}

	
	
	
	public static void main(String[] args) {
		int[] nums1 = {1,2,3,0,0,0};
		int[] nums2 = {2,5,6};
		
		int m = nums1.length;
		int n = nums2.length;
		
		System.out.println("Merged Arr:"+ merge(nums1, nums2, m, n));

	}

	
}
