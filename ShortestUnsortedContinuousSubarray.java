package LeetcodeArrayEasy;

import java.util.Stack;

/*581. Shortest Unsorted Continuous Subarray
Easy

1889

89

Favorite

Share
Given an integer array, you need to find one continuous subarray that if you only sort this subarray in ascending order, then the whole array will be sorted in ascending order, too.

You need to find the shortest such subarray and output its length.

Example 1:
Input: [2, 6, 4, 8, 10, 9, 15]
Output: 5
Explanation: You need to sort [6, 4, 8, 10, 9] in ascending order to make the whole array sorted in ascending order.
Note:
Then length of the input array is in range [1, 10,000].
The input array may contain duplicates, so ascending order here means <=.*/




public class ShortestUnsortedContinuousSubarray {

	private static int findUnsortedSubArray(int[] arr) {
		
		 Stack <Integer> stack = new Stack <Integer> ();
		 
		 int l=arr.length, r=0;
		 
	// left to right 
		 for(int i=0; i<arr.length; i++) {
			 
			 while(!stack.empty() && arr[stack.peek()] > arr[i]) {
				 l = Math.min(l, stack.pop());
			   }
			    stack.push(i);
			  
		  }
		     stack.clear();
		   
	   // right to left
		 for(int k=arr.length-1; k>=0; k--) { 
			 while(!stack.empty() && arr[stack.peek()] < arr[k]) {
				 r = Math.max(r, stack.pop());
			   }
			    stack.push(k);
			  
		  }
		     stack.clear();
		     
		     return r-l >0 ? r-l+1 : 0;
	}
	
	
	
	
	public static void main(String[] args) {
		int[] arr = {2, 6, 4, 8, 10, 9, 15};
		
		System.out.println(findUnsortedSubArray(arr));

	}

	

}
