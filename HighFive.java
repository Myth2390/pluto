package LeetcodeArrayEasy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/*1086. High Five
Easy

95

26

Favorite

Share
Given a list of scores of different students, return the average score of each student's top five scores in the order of each student's id.

Each entry items[i] has items[i][0] the student's id, and items[i][1] the student's score.  The average score is calculated using integer division.

 

Example 1:

Input: [[1,91],[1,92],[2,93],[2,97],[1,60],[2,77],[1,65],[1,87],[1,100],[2,100],[2,76]]
Output: [[1,87],[2,88]]
Explanation: 
The average of the student with id = 1 is 87.
The average of the student with id = 2 is 88.6. But with integer division their average converts to 88.
 

Note:

1 <= items.length <= 1000
items[i].length == 2
The IDs of the students is between 1 to 1000
The score of the students is between 1 to 100
For each student, there are at least 5 scores
Accepted
12,548
Submissions
16,564*/



public class HighFive {
	
	private static int[][] highFive(int[][] input) {
		HashMap<Integer, List> studentMarks = new HashMap<>();
		
		for(int[]item :input) {
			List marks = studentMarks.getOrDefault(item[0], new ArrayList<>());
			marks.add(item[1]);
			studentMarks.put(item[0], marks);
		}
			
			int[][] highFive = new int[studentMarks.size()][2];
			
			for(int id : studentMarks.keySet()) {
				
				List<Integer> score = studentMarks.get(id);
				
	/* Collections.sort method is sorting the elements of ArrayList in ascending order. */
		        Collections.sort(score, Collections.reverseOrder()); 
		        
   // calculate average
		        Integer sum = new Integer(0);
		        int irow =0;
		        
		        for(int s=0; s<5; s++) {
		        	sum = sum + score.get(s);
		        	
		        	highFive[irow][0] = id;
		        	highFive[irow][1] = sum/5;
		        }
				
			}
			
		
		return highFive;
	}
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       int[][] input = {{1,91},{1,92},{2,93},{2,97},{1,60},{2,77},{1,65},{1,87},{1,100},{2,100},{2,76}};
       
       System.out.println("highFive" + highFive(input));
	}

	

}







