package LeetcodeArrayEasy;

import java.util.Arrays;

public class ArrayPartitionI {
	
	
	
	private static int arrayPairSum(int[] arr) {
		
		Arrays.sort(arr);
		int sum =0;
		for(int i=0; i<arr.length; i=i+2) {
			
			sum = sum+arr[i];
			
			
		}
		return sum;
	}
	
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {1,4,3,2};
		System.out.println(arrayPairSum(arr));

	}

	

}
