package LeetcodeArrayEasy;

/* #idea    
 * a       b
[1,1]   [2,2]
SumA-x+y   =  SumB+x-y
-x+y-x+y=SumB-SumA
2y-2x=SumB-SumA
2(y-x)=SumB-SumA
y-x=SumB-SumA/2                             
y=x+(SumB-SumA)/2;

 [x,y]
 
 
*/

/*Hello making changes*/



public class FairCandySwap {
	
	private static int[] fairCandySwap(int[] A, int[] B) {
		int sumA = 0;
		int sumB = 0;
		int[] result = new int[2];
		
		for(int i : A) {
			sumA = sumA +i;
		}
		
		for(int j : B) {
			sumB = sumB + j;
		}
		
		// If Alice gives x, she expects to receive x + delta
		int delta = (sumB - sumA)/2;
		
		
		for(int i=0; i<A.length; i++) {
			for(int j=0; j<B.length; j++) {
				
				if(A[i] - B[j] == delta) {
					return result = new int[] {A[i], B[j]};
				}
			}
		}
		
		
		return result;
		
	}

	public static void main(String[] args) {
		int[] alice = {1,1};
		int[] bob = {2,2};
		
		System.out.println("Fair Candy::" + fairCandySwap(alice, bob));
	}

	

}}
